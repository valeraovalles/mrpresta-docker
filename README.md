# MRPRESTA DOCKER

***You must create following directories***

```sh
    mysql
    proyects/mrpresta
    proyects/gyramais
```

***install***

```sh
    docker
    docker-compose
```

***Initialize***

```sh
    In the main directory run this:
    sudo docker-compose build
    sudo docker-compose up -d
```
